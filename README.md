#TAA PROJECT 2015-2016
**by augustin bardou**

##Launch
To start the project, you just have to do a mvn clean install in the root of the project and launch the rest server. For this project I chose to work with Netbeans because I encoutered some issues to launch the server in command line or with IntelliJ.

To start the server, you have to execute the following command in the root of the project: http-server

The index page is in the src/main/webapp folder.

When the server is launched, you have to go on this address: localhost:8080/api with your favorite browser.
##Classes
*User*

*Task*

*Role*

*Stories*

##Dependancies
1 user can have 1 role and 1 role can have many user (OneToMany - ManyToOne)

1 user can have many task and 1 task can have many user (ManyToMany - ManyToMany)

1 task can have many stories and 1 stories can have 1 task (ManyToOne - OneToMany)

##Methods
I didn't do the docker's part and in my last version I encoutered some difficulties to create a user or a task because of the ManyToMany dependancy. I tried to fix it with an initialization of the list (list of task for the user and list of user for task)  but it doesn't work.
Otherwise, we can add stories and role and get them by id or by name.

