/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 *
 * @author augustin
 */
@Aspect
public class LogAspect {

    public LogAspect() {
    }
    
    //@Before("//PointCut expression")
    @Around("within(fr.istic.taa.jaxrs.web.rest")
    public Object log(ProceedingJoinPoint pjp) throws Throwable{
        System.err.println("/\\aspect/\\"+pjp.getSignature().getName());
        return pjp.proceed();
    }
}

