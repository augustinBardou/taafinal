/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Entity
@Component
public class Task {
    @Id
    @GeneratedValue
    long idTask;
    String descriptionTask;
    @Basic(fetch = LAZY)
    @ManyToMany(mappedBy = "taskListUser")
    @JsonBackReference
    List<User> userListTask;
    @Basic(fetch = LAZY)
    @ManyToOne
    Stories stories;

    public Task(String description){
        descriptionTask = description;
    }

    public Task() {
        userListTask = new ArrayList<>();
    }

    public Stories getStories() {return stories;}

    public long getIdTask() {return idTask;}

    public List<User> getUserListTask() {return userListTask;}

    public String getDescriptionTask() {return descriptionTask;}

    public void setDescriptionTask(String descriptionTask) {
        this.descriptionTask = descriptionTask;
    }

    public void setUserListTask(List<User> userListTask) {
        this.userListTask = userListTask;
    }

    public void setStories(Stories stories) {
        this.stories = stories;
    }
    
    public String toString(){
        return "Task[Id="+idTask+
                ", description="+descriptionTask+
                ", users="+getUserListTask()+
                ", stories="+getStories()+"]";
    }
}
