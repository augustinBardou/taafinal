/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Entity
@Component
public class User {
    @Id
    @GeneratedValue
    long idUser;
    String nameUser;
    String firstnameUser;
    @Basic(fetch = LAZY)
    @ManyToOne
    Role role;
    @Basic(fetch = LAZY)
    @ManyToMany
    @JoinTable(name="User_Task", 
            joinColumns = {
                @JoinColumn(name = "idUser", referencedColumnName = "idUser")},
            inverseJoinColumns = {
                @JoinColumn(name="idTask", referencedColumnName = "idTask")}
    )
    @JsonManagedReference
    List<Task> taskListUser;
    public User(){
        taskListUser = new ArrayList<>();
    }
    public User(String name, String firstname){
        nameUser = name;
        firstnameUser = firstname;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getFirstnameUser() {
        return firstnameUser;
    }

    public void setFirstnameUser(String firstnameUser) {
        this.firstnameUser = firstnameUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Task> getTaskListUser() {
        return taskListUser;
    }

    public void setTaskListUser(List<Task> taskListUser) {
        this.taskListUser = taskListUser;
    }
}
