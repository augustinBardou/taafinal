/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.domain;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author augustin
 */
@Entity
public class Stories {
     @Id
    @GeneratedValue
    long idStories;
    String nameStories;
    String descreptionStories;
    @OneToMany
    List<Task> taskListStories;

    public Stories(String name, String description){
        nameStories         = name;
        descreptionStories  = description;
    }

    public Stories() {
    }

    public long getIdStories() {return idStories;}

    public String getNameStories() {return nameStories;}

    public String getDescreptionStories() {return descreptionStories;}

    public List<Task> getTaskListStories() {return taskListStories;}

    public void setDescreptionStories(String descreptionStories) {
        this.descreptionStories = descreptionStories;
    }

    public void setNameStories(String nameStories) {
        this.nameStories = nameStories;
    }

    public void setTaskListStories(List<Task> taskListStories) {
        this.taskListStories = taskListStories;
    }
    
    public String toString(){
        return "Stories[Id="+idStories+
                ", name="+nameStories+
                ", description="+descreptionStories+
                ", tasks="+getTaskListStories()+"]";
    }
}
