/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author augustin
 */
@Entity
public class Role {
    @Id
    @GeneratedValue
    long    idRole;
    String  nameRole;
    String  descriptionRole;
    @OneToMany
    List <User> listUser;

    public Role(String name, String description){
        listUser        = new ArrayList<>();
        nameRole        = name;
        descriptionRole = description;
    }

    public Role() {
    }

    public long getIdRole() {return idRole;}

    public String getNameRole() {return nameRole;}

    public void setName(String name) {
        this.nameRole = name;
    }

    public String getDescriptionRole() {
        return descriptionRole;
    }

    public void setDescriptionRole(String descriptionRole) {
        this.descriptionRole = descriptionRole;
    }

    public List<User> getListUser() {return listUser;}

    public String showUsers(){
        String users = "";
        for(User e : listUser){
            users += e.idUser;
        }
        return users;
    }
    @Override
    public String toString(){
        return "Role[id="+idRole+
                ", name="+nameRole+
                ", description="+descriptionRole+
                ", user list="+showUsers()+"]";
    }
}
