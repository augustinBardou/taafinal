/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs;

import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Component
@Configuration
public class JerseyConfiguration extends ResourceConfig {
    
    public JerseyConfiguration(){
        
        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        
        packages("fr.istic.taa.jaxrs.web.rest");
        
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/");
        beanConfig.setResourcePackage("fr.istic.taa.jaxrs.web.rest");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
        
    }
    
}
