/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.repository;

import fr.istic.taa.jaxrs.domain.Role;
import fr.istic.taa.jaxrs.domain.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Component
public class UserRepository {
    private EntityManagerFactory factory;
    private EntityManager manager;
    private RoleRepository roleRepository;
    private TaskRepository taskRepository;
    public UserRepository(){
        
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }
    @Autowired
    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    public RoleRepository getRoleRepository() {
        return roleRepository;
    }
    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }
    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
    
    /**
     * Get one user with his name
     * @param name
     * @return a list of user
     */
    public List<User> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<User> user = new ArrayList<>();
        try {
            user = manager.createQuery("SELECT u from User u WHERE u.nameUser = :name", User.class)
                    .setParameter("name", name)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return user;
    }    
    
    /**
     * Find all users in the database
     * @return a list of user
     */
    public List<User> findAll(){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<User> users = new ArrayList<User>();
        try{
            users = manager.createQuery("Select u from User u", User.class).getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return users;
    }
    
    /**
     * Create a new entry user in the database
     * @param nameUser
     * @param firstnameUser
     * @return the new user
     */
    public User createUser(String nameUser, String firstnameUser){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        User newUser = new User(nameUser, firstnameUser);
        newUser.setTaskListUser(null);
        newUser.setRole(null);
        manager.persist(newUser);
        tx.commit();
        return newUser;
    }
    /**
     * Create a new entry user in the database
     * @param nameUser
     * @param firstnameUser
     * @param id
     * @return the new user with a role
     */
    public User createUserWithRole(String nameUser, String firstnameUser, long id){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Role> roles = roleRepository.findOneById(id);
        User newUser = new User(nameUser, firstnameUser);
        newUser.setRole(roles.get(0));
        manager.persist(newUser);
        tx.commit();
        return newUser;
    }
    
    /**
     * Get a list of user with an id
     * @param id
     * @return a list of user
     */
    public List<User> findOneById(Long id) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<User> user = new ArrayList<>();
        try {
            user = manager.createQuery("SELECT u from User u WHERE u.idUser = :id", User.class)
                    .setParameter("id", id)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return user;
    }
}
