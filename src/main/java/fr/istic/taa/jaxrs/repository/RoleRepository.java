/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.repository;

import fr.istic.taa.jaxrs.domain.Role;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Component
public class RoleRepository {
    
    //EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    // EntityManager manager = factory.createEntityManager();
    
    private EntityManagerFactory factory;
    private EntityManager manager;
    
    public RoleRepository (){
        
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }

    @Autowired
    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
        setManager(factory.createEntityManager());
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }
    
    /**
     * Find all roles in the database
     * @return list of role
     */
    public List<Role> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Role> roles = new ArrayList<Role>();
        try {
            roles = manager.createQuery("SELECT r from Role r", Role.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return roles;
    }
    /**
     * Create a new entry user in the database
     * @param name
     * @param description
     * @return the new story
     */
    public Role createRole(String name, String description){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Role newRole = new Role(name, description);
        manager.persist(newRole);
        tx.commit();
        return newRole;
    }
    
    /**
     * Get a list of role with a name
     * @param name
     * @return a list of role
     */
    public List<Role> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Role> role = new ArrayList<>();
        try {
            role = manager.createQuery("SELECT r from Role r WHERE r.nameRole = :name", Role.class)
                    .setParameter("name", name)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return role;
    }
    /**
     * Get a list of role with an id
     * @param id
     * @return a list of role
     */
    public List<Role> findOneById(Long id) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Role> role = new ArrayList<>();
        try {
            role = manager.createQuery("SELECT r from Role r WHERE r.idRole = :id", Role.class)
                    .setParameter("id", id)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return role;
    }
}
