/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.repository;

import fr.istic.taa.jaxrs.domain.Task;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Component
public class TaskRepository {
    private EntityManagerFactory factory;
    private EntityManager manager;
    private StoriesRepository storiesRepository;
    public TaskRepository(){
        
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }
    @Autowired
    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    public StoriesRepository getStoriesRepository() {
        return storiesRepository;
    }
    @Autowired
    public void setStoriesRepository(StoriesRepository storiesRepository) {
        this.storiesRepository = storiesRepository;
    }
    
    /**
     * Create a new entry task in the database
     * @param description
     * @return the task
     */
    public Task createTask(String description){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Task newTask = new Task(description);
        manager.persist(newTask);
        tx.commit();
        return newTask;
    }
    
    /**
     * Find all tasks in the database
     * @return a list of task
     */
    public List<Task> findAll(){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> tasks = new ArrayList<Task>();
        try{
            tasks = manager.createQuery("Select u from Task u", Task.class).getResultList();
        }catch(Exception e){
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return tasks;
    }
    
    /**
     * Get a list of task with an id
     * @param id
     * @return 
     */
    public List<Task> findOneById(long id) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Task> task = new ArrayList<>();
        try {
            task = manager.createQuery("SELECT t from Task t WHERE t.idTask = :id", Task.class)
                    .setParameter("id", id)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return task;
    }
}
