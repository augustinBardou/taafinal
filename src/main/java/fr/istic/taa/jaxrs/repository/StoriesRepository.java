/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.repository;

import fr.istic.taa.jaxrs.domain.Stories;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author augustin
 */
@Component
public class StoriesRepository {
    
    //EntityManagerFactory factory = Persistence.createEntityManagerFactory("dev");
    // EntityManager manager = factory.createEntityManager();
    
    private EntityManagerFactory factory;
    private EntityManager manager;
    
    public StoriesRepository (){
        
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }

    @Autowired
    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
        setManager(factory.createEntityManager());
    }

    public EntityManager getManager() {
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * Create a new entry user in the database
     * @param name
     * @param description
     * @return the new story
     */
    public Stories createStories(String name, String description){
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        Stories newStories = new Stories(name, description);
        manager.persist(newStories);
        tx.commit();
        return newStories;
    }
    
    /**
     * Get one stories with his name
     * @param name
     * @return a list of stories
     */
    public List<Stories> findOneByName(String name) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Stories> stories = new ArrayList<>();
        try {
            stories = manager.createQuery("SELECT s from Stories s WHERE s.nameStories = :name", Stories.class)
                    .setParameter("name", name)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return stories;
    }
    
    /**
     * Find all stories in the database
     * @return a list of stories
     */
    public List<Stories> findAll() {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Stories> stories = new ArrayList<Stories>();
        try {
            stories = manager.createQuery("SELECT s from Stories s", Stories.class).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return stories;
    }
    /**
     * Get a list of stories with an id
     * @param id
     * @return a list of stories
     */
    public List<Stories> findOneById(long id) {
        EntityTransaction tx = manager.getTransaction();
        tx.begin();
        List<Stories> stories = new ArrayList<>();
        try {
            stories = manager.createQuery("SELECT s from Stories s WHERE s.idStories = :id", Stories.class)
                    .setParameter("id", id)
                    .getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        tx.commit();
        manager.close();
        factory.close();
        return stories;
    }
}
