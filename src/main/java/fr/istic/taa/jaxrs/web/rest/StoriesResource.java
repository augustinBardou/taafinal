/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.web.rest;

import fr.istic.taa.jaxrs.domain.Stories;
import fr.istic.taa.jaxrs.repository.StoriesRepository;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author augustin
 */
@Path("/stories")
@Api("/stories")
@Component
@RestController
public class StoriesResource {
    private StoriesRepository storiesRepository;
    
    public StoriesResource() {
        System.err.println("JE SUIS INSTANCIE YAHOU");
    }

    public StoriesRepository getStoriesRepository() {
        return storiesRepository;
    }

    @Autowired
    public void setStoriesRepository(StoriesRepository storiesRepository) {
        this.storiesRepository = storiesRepository;
        System.err.println("ATTRIBUTION D'UN DEPARTEMENT REPOSITORY");
    }
    
    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Stories> getAll() {
        return storiesRepository.findAll();
    }
    
    @POST
    @Path("/createStories")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Stories createStories(@FormParam("name")String name,
            @FormParam("description")String description){
        Stories newStories = storiesRepository.createStories(name,description);
        return newStories;
    }
    
    @POST
    @Path("/findByName")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Stories> findOneByName(@FormParam("name") String name){
        List<Stories> stories = storiesRepository.findOneByName(name);
        return stories;
    }
    
    @POST
    @Path("/findById")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Stories> findOneById(@FormParam("id") long id){
        List<Stories> stories = storiesRepository.findOneById(id);
        return stories;
    }
}
