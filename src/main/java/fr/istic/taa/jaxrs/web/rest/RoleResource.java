/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.web.rest;

import fr.istic.taa.jaxrs.domain.Role;
import fr.istic.taa.jaxrs.repository.RoleRepository;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author augustin
 */
@Path("/role")
@Api("/role")
@Component
@RestController
public class RoleResource {
    private RoleRepository roleRepository;
    
    public RoleResource() {
        System.err.println("JE SUIS INSTANCIE YAHOU");
    }

    public RoleRepository getRoleRepository() {
        return roleRepository;
    }

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
        System.err.println("ATTRIBUTION D'UN DEPARTEMENT REPOSITORY");
    }
    
    @POST
    @Path("/createRole")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Role createRole(@FormParam("name")String name,
            @FormParam("description")String description){
        Role newRole = roleRepository.createRole(name,description);
        return newRole;
    }
    
    @GET
    @Path("findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Role> getAll() {
        return roleRepository.findAll();
    }
    
    @POST
    @Path("/findByName")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Role> findOneByName(@FormParam("name")String name){
        List<Role> role = roleRepository.findOneByName(name);
        return role;
    }
    
    @POST
    @Path("/findById")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Role> findOneById(@FormParam("id")long id){
        List<Role> role = roleRepository.findOneById(id);
        return role;
    }
}
