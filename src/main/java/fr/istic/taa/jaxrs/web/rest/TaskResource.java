/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.web.rest;

import fr.istic.taa.jaxrs.domain.Task;
import fr.istic.taa.jaxrs.repository.TaskRepository;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author augustin
 */
@Path("/task")
@Api("/task")
@Component
@RestController
public class TaskResource {
    private TaskRepository taskRepository;
    
    public TaskResource() {
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        System.err.println("ATTRIBUTION D'UN DEPARTEMENT REPOSITORY");
    }
    
    @POST
    @Path("/createTask")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Task createTask(@FormParam("description")String description){
        Task newTask = taskRepository.createTask(description);
        return newTask;
    }
    
    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getAll() {
        return taskRepository.findAll();
    }
    
    @POST
    @Path("/findById")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> findOneByName(@FormParam("id")long id){
        List<Task> tasks = taskRepository.findOneById(id);
        return tasks;
    }
}
