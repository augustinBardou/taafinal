/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.taa.jaxrs.web.rest;

import fr.istic.taa.jaxrs.domain.Role;
import fr.istic.taa.jaxrs.domain.User;
import fr.istic.taa.jaxrs.repository.UserRepository;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author augustin
 */
@Path("/user")
@Api("/user")
@Component
@RestController
public class UserResource {
    private UserRepository userRepository;
    
    public UserResource() {
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Autowired
    public void setDepartmentRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
        System.err.println("ATTRIBUTION D'UN DEPARTEMENT REPOSITORY");
    }
    
    @POST
    @Path("/createUser")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public User createUser(@FormParam("nameUser")String nameUser, 
            @FormParam("firstnameUser")String firstnameUser){
        User newUser = userRepository.createUser(nameUser, firstnameUser);
        return newUser;
    }
    
    @POST
    @Path("/createUserId")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public User createUserWithRole(@FormParam("nameUser")String nameUser, 
            @FormParam("firstnameUser")String firstnameUser,
            @FormParam("idRole")long id){
        User newUser = userRepository.createUserWithRole(nameUser, firstnameUser,id);
        return newUser;
    }
    
    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getAll() {
        return userRepository.findAll();
    }
    
    @POST
    @Path("/findByName")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findOneByName(@FormParam("name")String name){
        List<User> users = userRepository.findOneByName(name);
        return users;
    }
    
    @POST
    @Path("/findById")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findOneById(@FormParam("id")long id){
        List<User> users = userRepository.findOneById(id);
        return users;
    }
}
